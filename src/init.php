<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * `wp-blocks`: includes block type registration and related functions.
 *
 * @since 1.0.0
 */
function sallys_atomic_grid_cgb_block_assets() {
	// Styles.
	wp_enqueue_style(
		'sallys_atomic_grid-cgb-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-blocks' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: filemtime — Gets file modification time.
	);
	// Slick slider JS
	wp_enqueue_script(
		'slick-js',
		plugins_url( 'lib/slick-carousel/slick/slick.min.js', dirname( __FILE__) ),
		array( 'jquery' ), 
		null
	);
	// Slick slider CSS
	wp_enqueue_style(
		'slick-css',
		plugins_url( 'lib/slick-carousel/slick/slick.css', dirname( __FILE__) ),
		null
	);
	wp_register_script(
		'slick-init-js',
		plugins_url( 'dist/slider.init.js', dirname( __FILE__ ) ),
		array('slick-js'),
		null
	);

} // End function sallys_atomic_grid_cgb_block_assets().

// Hook: Frontend assets.
add_action( 'enqueue_block_assets', 'sallys_atomic_grid_cgb_block_assets' );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * `wp-blocks`: includes block type registration and related functions.
 * `wp-element`: includes the WordPress Element abstraction for describing the structure of your blocks.
 * `wp-i18n`: To internationalize the block's text.
 *
 * @since 1.0.0
 */
function sallys_atomic_grid_editor_assets() {
	// Scripts.
	wp_enqueue_script(
		'sallys_atomic_grid-cgb-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element' ), // Dependencies, defined above.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
		true // Enqueue the script in the footer.
	);

	// Styles.
	wp_enqueue_style(
		'sallys_atomic_grid-cgb-block-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: filemtime — Gets file modification time.
	);
} // End function sallys_atomic_grid_cgb_editor_assets().


// Add custom block category
add_filter(
	'block_categories', function( $categories, $post ) {
		return array_merge(
			$categories,
			array(
				array(
					'slug'  => 'sallys-atomic-blocks',
					'title' => __( 'Sally\'s Atomic Blocks', 'sallys-atomic-blocks' ),
				),
			)
		);
	}, 10, 2
);

add_action( 'enqueue_block_editor_assets', 'sallys_atomic_grid_editor_assets' );


/**
 * Registers the `core/latest-posts` block on server.
 */
function register_block_sallys_atomic_grid() {

	// Check if the register function exists
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
	}

	register_block_type(
		'sallys-atomic-blocks/sallys-atomic-grid', array(
			'attributes'      => array(
				'categories'         => array(
					'type' => 'string',
				),
				'className'          => array(
					'type' => 'string',
				),
				'postsToShow'        => array(
					'type'    => 'number',
					'default' => 6,
				),
				'displayPostDate'    => array(
					'type'    => 'boolean',
					'default' => true,
				),
				'displayPostExcerpt' => array(
					'type'    => 'boolean',
					'default' => true,
				),
				'displayPostAuthor'  => array(
					'type'    => 'boolean',
					'default' => true,
				),
				'displayPostImage'   => array(
					'type'    => 'boolean',
					'default' => true,
				),
				'displayPostLink'    => array(
					'type'    => 'boolean',
					'default' => true,
				),
				'postLayout'         => array(
					'type'    => 'string',
					'default' => 'grid',
				),
				'columns'            => array(
					'type'    => 'number',
					'default' => 2,
				),
				'align'              => array(
					'type'    => 'string',
					'default' => 'center',
				),
				'width'              => array(
					'type'    => 'string',
					'default' => 'wide',
				),
				'order'              => array(
					'type'    => 'string',
					'default' => 'desc',
				),
				'orderBy'            => array(
					'type'    => 'string',
					'default' => 'date',
				),
				'imageCrop'          => array(
					'type'    => 'string',
					'default' => 'landscape',
				),
			),
			'render_callback' => 'render_block_sallys_atomic_grid',
		)
	);
}

add_action( 'init', 'register_block_sallys_atomic_grid' );


/**
 * Renders the post grid block on server.
 */
function render_block_sallys_atomic_grid( $attributes ) {

	$recent_posts_query = new WP_Query(
		array(
			'numberposts' => $attributes['postsToShow'],
			'post_type'   => 'client',
			'post_status' => 'publish',
			'order'       => $attributes['order'],
			'orderby'     => $attributes['orderBy'],
			'category'    => $attributes['categories'],
		)
	);

	if ( ! $recent_posts_query->have_posts() ) {
		return;
	}

	$recent_posts = $recent_posts_query->posts;

	$list_items_markup = '';

	foreach ( $recent_posts as $post ) {
		// Get the post ID
		$post_id = $post->ID;

		// Get the post thumbnail
		$post_thumb_id = get_post_thumbnail_id( $post_id );

		if ( $post_thumb_id && isset( $attributes['displayPostImage'] ) && $attributes['displayPostImage'] ) {
			$post_thumb_class = 'has-thumb';
		} else {
			$post_thumb_class = 'no-thumb';
		}

		// Start the markup for the post
		$list_items_markup .= sprintf(
			'<article class="%1$s">',
			esc_attr( $post_thumb_class )
		);

		// Get the featured image
		if ( isset( $attributes['displayPostImage'] ) && $attributes['displayPostImage'] && $post_thumb_id ) {
			if ( $attributes['imageCrop'] === 'landscape' ) {
				$post_thumb_size = 'ab-block-post-grid-landscape';
			} else {
				$post_thumb_size = 'ab-block-post-grid-square';
			}

			$list_items_markup .= sprintf(
				'<div class="ab-block-post-grid-image"><a href="%1$s" rel="bookmark">%2$s</a></div>',
				esc_url( get_permalink( $post_id ) ),
				wp_get_attachment_image( $post_thumb_id, $post_thumb_size )
			);
		}

		// Wrap the text content
		$list_items_markup .= sprintf(
			'<div class="ab-block-post-grid-text">'
		);

			// Get the post title
			$title = get_the_title( $post_id );

		if ( ! $title ) {
			$title = __( 'Untitled', 'atomic-blocks' );
		}

			$list_items_markup .= sprintf(
				'<h2 class="ab-block-post-grid-title"><a href="%1$s" rel="bookmark">%2$s</a></h2>',
				esc_url( get_permalink( $post_id ) ),
				esc_html( $title )
			);

			// Wrap the byline content
			$list_items_markup .= sprintf(
				'<div class="ab-block-post-grid-byline">'
			);

				// Get the post author
		if ( isset( $attributes['displayPostAuthor'] ) && $attributes['displayPostAuthor'] ) {
			$list_items_markup .= sprintf(
				'<div class="ab-block-post-grid-author"><a class="ab-text-link" href="%2$s">%1$s</a></div>',
				esc_html( get_the_author_meta( 'display_name', $post->post_author ) ),
				esc_html( get_author_posts_url( $post->post_author ) )
			);
		}

				// Get the post date
		if ( isset( $attributes['displayPostDate'] ) && $attributes['displayPostDate'] ) {
			$list_items_markup .= sprintf(
				'<time datetime="%1$s" class="ab-block-post-grid-date">%2$s</time>',
				esc_attr( get_the_date( 'c', $post_id ) ),
				esc_html( get_the_date( '', $post_id ) )
			);
		}

			// Close the byline content
			$list_items_markup .= sprintf(
				'</div>'
			);

			// Wrap the excerpt content
			$list_items_markup .= sprintf(
				'<div class="ab-block-post-grid-excerpt">'
			);

				// Get the excerpt
				$excerpt = apply_filters( 'the_excerpt', get_post_field( 'post_excerpt', $post_id, 'display' ) );

		if ( empty( $excerpt ) ) {
			$excerpt = apply_filters( 'the_excerpt', wp_trim_words( $post->post_content, 55 ) );
		}

		if ( ! $excerpt ) {
			$excerpt = null;
		}

		if ( isset( $attributes['displayPostExcerpt'] ) && $attributes['displayPostExcerpt'] ) {
			$list_items_markup .= wp_kses_post( $excerpt );
		}

		if ( isset( $attributes['displayPostLink'] ) && $attributes['displayPostLink'] ) {
			$list_items_markup .= sprintf(
				'<p><a class="ab-block-post-grid-link ab-text-link" href="%1$s" rel="bookmark">%2$s</a></p>',
				esc_url( get_permalink( $post_id ) ),
				esc_html__( 'Continue Reading', 'atomic-blocks' )
			);
		}

			// Close the excerpt content
			$list_items_markup .= sprintf(
				'</div>'
			);

		// Wrap the text content
		$list_items_markup .= sprintf(
			'</div>'
		);

		// Close the markup for the post
		$list_items_markup .= "</article>\n";
	}

	// Build the classes
	$class = "ab-block-post-grid align{$attributes['align']}";

	if ( isset( $attributes['className'] ) ) {
		$class .= ' ' . $attributes['className'];
	}

	$grid_class = 'ab-post-grid-items';

	if ( isset( $attributes['postLayout'] ) && 'list' === $attributes['postLayout'] ) {
		$grid_class .= ' is-list';
	} else {
		$grid_class .= ' is-grid';
	}

	if ( isset( $attributes['columns'] ) && 'grid' === $attributes['postLayout'] ) {
		$grid_class .= ' columns-' . $attributes['columns'];
	}

	// Output the post markup
	$block_content = sprintf(
		'<div class="%1$s"><div class="%2$s">%3$s</div></div>',
		esc_attr( $class ),
		esc_attr( $grid_class ),
		$list_items_markup
	);

	return $block_content;

}

/**
 * Create API fields for client and service post types.
 **/
function sallys_atomic_grid_register_rest_fields() {

	if ( function_exists( 'atomic_blocks_get_image_src_landscape' ) ) {
		// Add landscape featured image source.
		register_rest_field(
			array( 'client', 'service' ),
			'featured_image_src',
			array(
				'get_callback'    => 'atomic_blocks_get_image_src_landscape',
				'update_callback' => null,
				'schema'          => null,
			)
		);
	}

	if ( function_exists( 'atomic_blocks_get_image_src_square' ) ) {
		// Add square featured image source.
		register_rest_field(
			array( 'client', 'service' ),
			'featured_image_src_square',
			array(
				'get_callback'    => 'atomic_blocks_get_image_src_square',
				'update_callback' => null,
				'schema'          => null,
			)
		);
	}

	if ( function_exists( 'atomic_blocks_get_author_info' ) ) {
		// Add author info.
		register_rest_field(
			array( 'client', 'service' ),
			'author_info',
			array(
				'get_callback'    => 'atomic_blocks_get_author_info',
				'update_callback' => null,
				'schema'          => null,
			)
		);
	}
}

add_action( 'rest_api_init', 'sallys_atomic_grid_register_rest_fields' );
