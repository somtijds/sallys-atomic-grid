/**
 * BLOCK: sallys-atomic-grid
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { ServerSideRender } = wp.components;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'sallys-atomic-blocks/block-sallys-atomic-grid', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Sally\'s Client / Services Grid' ), // Block title.
	icon: 'grid', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'sallys-atomic-blocks', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'sallys-atomic-grid' ),
		__( 'clients' ),
		__( 'services' ),
	],

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( props ) {

		const { content } = props.attributes;

        function onChangeContent( newContent ) {
            setAttributes( { content: newContent } );
        }

		return (

			<div className={ props.className }>
				<p>
					CGB BLOCK: <code>sallys-atomic-grid</code> is a new Gutenberg block.
				</p>
		        <ServerSideRender
            		block="sallys-atomic-blocks/sallys-atomic-grid"
            		attributes={props.attributes}
        		/>
			</div>

		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: () => {
		
		return null;
		
	},
} );
