<?php 

add_shortcode( 'sallys_services', 'action_render_sallys_services' );

function action_render_sallys_services( $atts ) {

	if ( empty( $atts ) ) { 
		return sallys_get_services();
	}
	
}

function sallys_get_services_by_ids() {

}

function sallys_get_services() {
	$services_args = array(
		'post_type'      => 'service',
		'post_status'    => 'publish',
		'posts_per_page' => '8',
		'orderby'		 => 'menu_order',
		'order'          => 'ASC',
	);

	$services_query = new WP_Query( $services_args );

	if ( $services_query->have_posts() ) {
		ob_start(); ?>
		<div class="services-grid">
		<?php
			while ( $services_query->have_posts() ) {
				$services_query->the_post();
				get_template_part( 'template-parts/loop-single-service' );	
			}
		?>
		</div>
		<?php return ob_get_clean();
	} else {
		return 'no services';
	}
}

add_shortcode( 'sallys_clients', 'action_render_sallys_clients' );

function action_render_sallys_clients( $atts ) {

	wp_enqueue_script( 'slick-init-js');

	if ( empty( $atts ) ) { 
		return sallys_get_clients();
	}
	
}

function sallys_get_clients() {
	$clients_args = array(
		'post_type'      => 'client',
		'post_status'    => 'publish',
		'posts_per_page' => '80',
		'orderby'		 => 'menu_order',
		'order'          => 'ASC',
	);
	$clients_query = new WP_Query( $clients_args );

	if ( $clients_query->have_posts() ) {
		ob_start(); ?>
		<div class="client-slider" data-slick='{"slidesToView": 1, "dots": true,"arrows": false,"speed": 300,"infinite": false}'>
		<?php
			$griditem_index = 0;
			while ( $clients_query->have_posts() ) {
				$griditem_index++;
				$clients_query->the_post();
				$whereami = $griditem_index % 20;
				if ( 1 === $griditem_index || 1 === $whereami ) : ?>
					<div class="client-slider__item clients-grid">
				<?php endif;
				
				get_template_part( 'template-parts/loop-single-client' );
				
				if ( 0 === $whereami ) : ?>
					</div><!--client-slider__item-->
				<?php endif;
			}
		?>
		</div><!--client-slider-->
		<?php return ob_get_clean();
	} else {
		return 'no clients';
	}
}